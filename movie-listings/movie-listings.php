<?php
/**
*
* Plugin Name: Movie Listings
* Description: Lists movies and info.
* Version: 1.0
* Author: Big Rocket Games LLC
*
**/

// Exit if Accessed Directly
if(!defined('ABSPATH')){
	exit();
};

// Load Scripts
require_once(plugin_dir_path(__FILE__).'/includes/movie-listings-scripts.php');

// Load Files needed for plugin
require_once(plugin_dir_path(__FILE__).'/includes/movie-listings-settings.php');
require_once(plugin_dir_path(__FILE__).'/includes/movie-listings-cpt.php');
require_once(plugin_dir_path(__FILE__).'/includes/movie-listings-fields.php');
require_once(plugin_dir_path(__FILE__).'/includes/movie-listings-reorder.php');
require_once(plugin_dir_path(__FILE__).'/includes/movie-listings-shortcodes.php');

?>