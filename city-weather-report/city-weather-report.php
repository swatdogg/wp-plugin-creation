<?php
/**
*
* Plugin Name: City Weather Report
* Description: Display users city weather report in widget.
* Version: 1.0
* Author: Big Rocket Games LLC
*
**/

// Exit if Accessed Directly
if(!defined('ABSPATH')){
	exit();
};

// Include Scripts
require_once(plugin_dir_path(__FILE__) . '/includes/city-weather-report-scripts.php');

// Load Geoplugin
require_once(plugin_dir_path(__FILE__) . '/includes/geoplugin.class.php');

// Include Class
require_once(plugin_dir_path(__FILE__) . '/includes/city-weather-report-class.php');

// Register Widget
function register_city_weather_report(){
	register_widget('City_Weather_Report_Widget');
}

add_action('widgets_init', 'register_city_weather_report');

?>