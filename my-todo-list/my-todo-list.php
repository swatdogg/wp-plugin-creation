<?php
/**
*
* Plugin Name: My ToDo List
* Description: A simple plugin to let you create your own todo list.
* Version: 1.0
* Author: Big Rocket Games LLC
*
**/

// Exit if Accessed Directly
if(!defined('ABSPATH')){
	exit();
};

// Global Options Variable
$ffl_options = get_option('ffl_settings');

// Load Scripts
require_once(plugin_dir_path(__FILE__).'/includes/my-todo-list-scripts.php');

// Load Shortcodes
require_once(plugin_dir_path(__FILE__).'/includes/my-todo-list-shortcodes.php');

// Check if admin
if(is_admin()){
	// Load Custom Post Type
	require_once(plugin_dir_path(__FILE__).'/includes/my-todo-list-cpt.php');

	// Load Custom Fields
	require_once(plugin_dir_path(__FILE__).'/includes/my-todo-list-fields.php');
};

?>