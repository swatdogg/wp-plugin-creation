<?php
/**
*
* Plugin Name: Instagram Photo List
* Description: Shows latest Instagram photos.
* Version: 1.0
* Author: Big Rocket Games LLC
* Author URI: http://www.bigrocketgames.com
*
**/

// Exit if Accessed Directly
if(!defined('ABSPATH')){
	exit();
};

// Global Options Variable
$ipl_options = get_option('ipl_settings');

// Load Scripts
require_once(plugin_dir_path(__FILE__).'/includes/instagram-photo-list-scripts.php');

// Load Shortcodes
require_once(plugin_dir_path(__FILE__).'/includes/instagram-photo-list-shortcodes.php');

// Check if admin and include admin scripts
if(is_admin()){
	// Load Settings
	require_once(plugin_dir_path(__FILE__).'/includes/instagram-photo-list-settings.php');
};

?>